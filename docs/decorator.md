<div align="center">
  <h1>Decorator</h1>
</div>

<div align="center">
  <img src="decorator_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Decorator is a structural pattern for wrapper objects that attach additional behaviour to
existing objects.**

### Real-World Analogy

_Protective clothing._

A warm sweater, raincoat and helmet are examples of things you can wear that modify your abilities.
You get a combined effect from wearing multiple items.

### Participants

- :bust_in_silhouette: **Component**
    - Defines an interface for:
        - `operation`

- :man: **ConcreteComponent**
    - Provides an implementation for:
        - `operation`

- :bust_in_silhouette: **Decorator**
    - Maintains a reference to a Component object.
    - Defines an abstract method to:
        - `operation`

- :man: **ConcreteDecorator**
    - Provides an implementation for:
        - `operation`: adding extra behavior to the method.

### Collaborations

...
Decorator forwards requests to its Component object. It may optionally perform additional operations before and after
forwarding the request.

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When you need to add behavior to individual objects dynamically. Or, when extension by
subclassing is either impractical or impossible.**

### Motivation

- How do we add responsibilities to individual objects, not to an entire class?
- How do we add responsibilities that can later be withdrawn?
- How do we extend the behavior if subclassing is impractical because it leads to a class explosion or when the class
  definition is unavailable for subclassing?

### Known Uses

- Graphical User Interfaces (GUIs):
    - Adding scrollbars to a text box and/or borders to a window.
    - In text processing applications, decorators can be used to apply different formatting styles (e.g., bold, italic,
      underline) to text elements.
- Crosscutting concerns:
    - Add notification mechanisms to objects, allowing them to send notifications or events to observers.
    - Log method calls, measure the execution time of methods, or add debugging information
    - Collect and send metrics or monitoring data to track the performance and usage of your API or web application.
- Preprocess request:
    - Validate input data before passing it to a method or operation, ensuring data integrity and correctness.
    - parsing custom headers or parameters before it reaches the request handler.
- Modify response:
    - Add internationalization and Localization to display content in different languages or regions.
    - Change the format of the response data, such as converting data to JSON or XML, based on the client's requested
      content type.
    - Determine the appropriate response content type based on the client's requested content type.
    - Add security headers to responses, such as Content Security Policy (CSP) or Cross-Origin Resource Sharing (CORS)
      headers.
    - Compress response data before sending it to the client to save bandwidth and improve load times.
- Providing options:
    - In a game, modify the damage output of a weapon by adding multiple enchantments and magic properties.
    - Dynamic Web Page Generation:a base page class and use decorators to add features like headers, footers, navigation
      menus, or authentication checks. This allows us to create different page variations with a combination of
      decorators.
    - Configurators: For example, in a vehicle configurator application, we can have a base vehicle object, and
      decorators can add options like sunroofs, leather seats, upgraded stereo systems
    - When all discounts have been applied, ensure discounts with an amount of 0 are discarded.

### Categorization

Purpose:  **Structural**  
Scope:    **Object**   
Mechanisms: **Polymorphism**, **Composition**

Structural patterns are concerned with how classes and objects are composed to form larger structures.

Structural object patterns describe ways to compose objects to realize new functionality.

The Decorator lets you structure your business logic into layers, create a decorator for each layer and compose objects
with various combinations of this logic at runtime. The client code can treat all these objects in the same way, since
they all follow a common interface.

### Aspects that can vary

- Behavior of an object without subclassing.

### Solution to causes of redesign

- Extending functionality by subclassing.
    - Hard to understand: comprehending a subclass requires in-depth knowledge of all parent classes.
    - Hard to change: a modification to a parent might break multiple children classes.
    - Hard to reuse partially: all class members are inherited, even when not applicable, which can lead to a class
      explosion.

- Inability to alter classes conveniently.
    - e.g: classes from a commercial class library
    - or the change would require modifying many existing subclasses

### Consequences

| Advantages                                                                                                                                                                                                                                                         | Disadvantages                                                                                                                                                                   |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Dynamic.** <br> With decorators, responsibilities can be added and removed at run-time simply by attaching and detaching them.                                                                                                                | :x: **Unreliable object identity.** <br> A decorator and its component don't have the same identy. If client code relies on that, it will break.                                |
| :heavy_check_mark: **No class explosion.** <br> We don't need a subclass for each class we need to add the behavior to.                                                                                                                                            | :x: **Lots of little objects.** <br> Although systems using decorators are easy to customize by those who understand them, they can be hard to learn and debug.                 |
| :heavy_check_mark: **Flexible.** <br> Providing different Decorators for a Component lets you mix and match behavior. Decorators also make it easy to add a property twice. For example: to give a TextView a double border.                                       | :x: **Hard to remove.** <br> It’s difficult to remove a specific wrapper from the wrappers stack.                                                                               |
| :heavy_check_mark: **Avoid big classes high in the inheritance hierarchy.** <br> Instead of trying to support all foreseeable features in a complex, customizable class, you can define a simple class and add functionality incrementally with Decorator objects. | :x: **Order matters.** <br> It’s hard to implement a decorator in such a way that its behavior doesn't depend on the order in the decorators stack                              |
| :heavy_check_mark: **No class modification required.** <br> Decorators can be applied to existing classes or legacy code without modifications, making it ideal for modifying legacy systems and 3rd party libraries.                                              | :x: **Complex Object Initialization.** <br> The initial configuration code of layers might become quite complex.                                                                |
| :heavy_check_mark: **Transparency.** <br> Clients interact with the decorated objects as if they were the original ones. This simplifies client code.                                                                                                              | :x: **Works better with small interfaces.** <br> Since a decorator needs to wrap all methods of a class to be transparent, interfaces with many methods lead to big decorators. |
|                                                                                                                                                                                                                                                                    | :x: **Potential for Unintended Behavior.** <br> If decorators are not designed carefully, they can lead to unintended behavior or conflicts between decorators.                 |

### Relations with Other Patterns

_Distinction from other patterns:_

- The strategy pattern allows you to modify the object's behavior by changing its internals (changing the guts).
    - Decorator changes behavior of an object by wrapping it from the outside (changing its skin).
    - Strategy is a better choice:
        - If the object has many methods (which would otherwise all have to be wrapped by the decorator).
        - When the methods must differ from the component. A strategy can have its own specialized interface, whereas a
          decorator's interface must conform to the component's.
    - Decorators are the better choice if it's hard or impossible to modify the object itself (such that it would accept
      a strategy).
- A decorator is different from an adapter in that a decorator only changes an object's responsibilities, not its
  interface
    - an adapter will give an object a completely new interface.
- Decorator and Proxy have similar structures, but very different intents.
    - The Proxy pattern controls access to the object it contains, while the decorator adds behavior.
- A decorator can be viewed as a degenerate composite with only one component.
    - However, a decorator adds additional responsibilities; it isn't intended for object aggregation.
- Chain of Responsibility and Decorator have very similar class structures. Both patterns rely on recursive composition
  to pass the execution through a series of objects. However, there are several crucial differences.
    - The Chain of Responsibility handlers can execute arbitrary operations independently of each other.
    - They can also stop passing the request further at any point.
    - Various Decorators can extend the object’s behavior while keeping it consistent with the base interface.
    - In addition, decorators aren’t allowed to break the flow of the request.
- Composite and Decorator have similar structure diagrams since both rely on recursive composition to organize an
  open-ended number of objects.
    - A Decorator is like a Composite but only has one child component.
    - Decorator adds additional responsibilities to the wrapped object
    - Composite just “sums up” its children’s results.

_Combination with other patterns:_

- Decorators are typically created by other patterns like Factory and Builder
    - Then the creation of the concrete component with its decorator is “well encapsulated” and cant lead to the client
      forgetting to add required decorators.
- Decorator is often used with Composite.
    - When decorators and composites are used together, they will usually have a common parent class. So decorators will
      have to support the Component interface with operations like Add, Remove, and GetChild.
    - The Decorator can extend the behavior of a specific object in the Composite tree.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **A Decorator class that mimics the interface of the wrapped object and adds behavior to calls to
that object.**

### Structure

```mermaid
classDiagram
    class Component {
        <<interface>>
        + operation()
    }

    class ConcreteComponent {
        + operation()
    }

    class Decorator {
        <<abstract>>
        - component: Component
        + operation()
    }

    class ConcreteDecorator {
        + operation()
        + addedBehavior()
    }

    Component <|.. ConcreteComponent: implements
    Component <|.. Decorator: implements
    Decorator <|-- ConcreteDecorator: extends
    Component --* Decorator: composes
```

### Variations

- **Omitted abstract Decorator class**: There's no need to define an abstract Decorator class when you only need to add
  one behavior.
    - :heavy_check_mark: Simpler.
    - :x: Requires refactoring when more than one behavior becomes required.

### Implementation

In this example, we apply the decorator pattern to an e-commerce system that allows multiple types of discount.
Using the decorator pattern, we ensure that a discount for 0 euro's is not displayed, and that the discount can never
exceed the total price.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Decorator](https://refactoring.guru/design-patterns/decorator)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ZoranHorvat: Combine Design Patterns To Reveal Their Greatest Power](https://youtu.be/b1JyDzCe9n8?si=GAPwckCNdZzzo_Zo)
- [ZoranHorvat: Use the Decorator Pattern To Reduce Code Duplication in Complex Models](https://youtu.be/7N0LpGAI5T4?si=Fr0pY93veN_WMmoI)
- [ZoranHorvat: This Decorator Pattern Implementation Will Make Your Day!](https://youtu.be/Pqow_rfuZSU?si=b678SvJ2tG3_SJFX)
- [ArjanCodes: Python-decorator: the complete guide](https://youtu.be/QH5fw9kxDQA?si=XQVslshf7iEZkcmp)
- [Geekific: The Decorator Pattern Explained and Implemented in Java](https://youtu.be/v6tpISNjHf8?si=dJa5cdD7puSqAwdj)
- [pentalog: decorator-design-pattern](https://www.pentalog.com/blog/design-patterns/decorator-design-pattern/)

<br>
<br>
